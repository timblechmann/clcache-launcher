# `clcache` Launcher

Small helper tool to simplify the use of `clcache` with `cmake`:

TL;DR: `cmake -DCMAKE_CXX_COMPILER_LAUNCHER=/path/to/clcache-launcher.exe`


## `clcache`/`cmake` integration

`clcache` is designed to be used as drop-in replacement for `cl.exe`. it can be configured to use a specific version of `cl.exe` by using environment variables.
`cmake` however provides a notion of a "compiler launcher": a launcher program which takes the compiler executable as first argument, an API provided by well-known unix tools like `ccache` or `icecc`.

The launcher fills the gap of allowing `clcache` to be used from `cmake`.
