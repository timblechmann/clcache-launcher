use std::env;

use subprocess::{Exec, ExitStatus::Exited};

fn parse_result(status: subprocess::ExitStatus) -> i32 {
    match status {
        Exited(code) => code as i32,
        _ => 1,
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();

    if let Some((_, rest)) = args.split_first() {
        if let Some((compiler, arguments)) = rest.split_first() {
            let env = vec![("CLCACHE_CL".to_string(), compiler)];
            let exec = Exec::cmd("clcache.exe").args(arguments).env_extend(&env);

            let exit_status = exec.join();
            let status = match exit_status {
                Ok(status) => parse_result(status),
                Err(error) => {
                    eprintln!("clcache launcher error {:?}", error);
                    1
                }
            };
            std::process::exit(status)
        };
    }

    eprintln!("clcache launcher error: bad command line");

    std::process::exit(1)
}
